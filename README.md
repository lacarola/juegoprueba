# Juego de Eliminar Objetos

Este es un juego simple donde el objetivo es eliminar objetos haciendo clic en ellos. Cada vez que eliminas un objeto, ganas puntos.
Creado por Ana Carolina Angel.

## Requisitos

- Python 3.x

## Instalación

1. Asegúrate de tener Python 3.x instalado. Si no lo tienes, puedes descargarlo desde [python.org](https://www.python.org/).

## Ejecución

1. Clona o descarga este repositorio en tu máquina local.
2. Navega hasta el directorio donde has descargado el repositorio.
3. Ejecuta el siguiente comando en tu terminal o símbolo del sistema para iniciar el juego:

python elimina_objetos.py


## Cómo Jugar

- Escribe tu nombre 
- Selecciona la dificultad
- Haz clic en jugar
- Haz clic en los objetos que aparezcan en la pantalla para eliminarlos.
- Cada vez que eliminas un objeto, ganas puntos.
- Trata de eliminar tantos objetos como puedas para obtener la puntuación más alta.
- El juego termina cuando decides salir.

¡Diviértete jugando!

