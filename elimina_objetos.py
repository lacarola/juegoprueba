# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 16:13:54 2024

@author: Ana Carolina Angel
"""

import pygame
import pygame_menu
import random


# Definición de colores
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
PURPLE = (128, 0, 128)

pygame.init()
WIDTH, HEIGHT = 800, 600
screen = pygame.display.set_mode((800,600))
pygame.display.set_caption("Juego de Eliminar Objetos")
# Reloj para controlar la velocidad de actualización
clock = pygame.time.Clock()

# Clase para los objetos a eliminar
class GameObject(pygame.sprite.Sprite):
    def __init__(self, color, radius):
        super().__init__()
        self.color = color
        self.radius = radius
        self.image = pygame.Surface((radius*2, radius*2), pygame.SRCALPHA)
        pygame.draw.circle(self.image, self.color, (radius, radius), radius)
        self.rect = self.image.get_rect(center=(random.randint(radius, WIDTH-radius), random.randint(radius, HEIGHT-radius)))

# Grupo de todos los objetos
all_sprites = pygame.sprite.Group()

# Función para generar un nuevo objeto
def generate_object():
    print("genera")
    color = random.choice([RED, PURPLE, BLUE])
    radius = random.randint(20, 50)
    new_object = GameObject(color, radius)
    all_sprites.add(new_object)


def set_difficulty(value, difficulty):
    if value == 1:
        clock.tick(10) 
    elif value == 2:
        clock.tick(2) 
    

def start_the_game():
    print("inicia")
    active = True
    # Variable para los puntos   
    score = 0    
    while active:
        print(active)
        generate_object()
        # Control de eventos
        for event in pygame.event.get():
            if event.type == pygame.QUIT:            
                active = False
            elif event.type == pygame.MOUSEBUTTONDOWN:            
                # Verificar si se hizo clic en un objeto
                for obj in all_sprites:
                    if obj.rect.collidepoint(event.pos):
                        obj.kill()
                        score += 1
                        generate_object()  # Generar un nuevo objeto al eliminar uno
                        

        # Fondo de la pantalla
        screen.fill(WHITE)
        
        # Actualizar y dibujar todos los objetos
        all_sprites.update()
        all_sprites.draw(screen)

        # Mostrar puntuación en la pantalla
        font = pygame.font.Font(None, 28)
        name = menu.get_input_data().get('Nickname :')
        print(name)
        text = font.render(" Puntos: " + str(score), True, BLUE)
        screen.blit(text, (10, 10))

        # Actualizar la pantalla
        pygame.display.flip()

        # Control de velocidad
        clock.tick(2)
      
    
menu = pygame_menu.Menu('Bienvenid@', 800, 600,
                       theme=pygame_menu.themes.THEME_BLUE)

menu.add.text_input('Nickname: ', default='Pepito Pérez')
menu.add.selector('Modo Dificultad: ', [('Dificil', 1), ('Fácil', 2)], onchange=set_difficulty)
menu.add.button('Jugar', start_the_game)
menu.add.button('Salir', pygame_menu.events.EXIT)
menu.mainloop(screen)

pygame.quit()



